# Mubaloo Technical Test
## Owen Jones <owencjones@gmail.com>

My contribution for the Mubaloo technical test.

## Notes

* The form records to PutsReq.com at this url (https://putsreq.com/DzyZpdaH94kFVaKBFehZ/inspect)
* Just in case this is being assessed on time taken, I did this in little fragments, so the distance of two days from first to last commit is inaccurate - it took a couple of hours overall.
* The gulp command to serve below runs the app as a BrowserSync server, so you may prefer to build it, and view it in /dist, which will be a standalone version.

There are a couple of things I hoped to do, but that family life didn't allow me time for over the weekend!  See below.

* Proper form validation.  Intended to make use of ng-minlength and other directives of this nature, as it is I'm sticking with native form validation.
* Transitions between pages.  You may notice that one of the commits is removing the 'ng-animate' dependency, because I didn't really have time to use it.
* Reverse Geocoded locations using Google Maps (Using navigator.geolocation and GMaps API to make it into an address) - couldn't be used because it's not a packaged app as yet, so the API setup would have been prohibitive (it needs to know what domain it will be called from)

Other than that, I hope you like the result.

## Installation/running
The repo uses gulp as a task runner, so after you have pulled the contents of it into a useful location for you (clone the repo) then you should run the following

    npm install
    
To install all dependencies, then

    gulp serve
    
to run in a local server, or 

    gulp build
    
to compile a distribution copy of the app to /dist.

Enjoy.
