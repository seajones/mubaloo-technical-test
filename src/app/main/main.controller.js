'use strict';

angular.module('mubaloo')
  .controller('MainCtrl', function ($scope, $http) {

    this.currentStep = 1;

    this.tried = false;

    /**
     * Setter for 'tried' variable - defines whether form submit has been tried already
     * @param val
     */
    this.triedSet = function(val) {
      this.tried = val;
    }

    /**
     * Setter for current step of form - controls which page shows currently.
     * @param val
     */
    this.stepSet = function (val) {
      this.currentStep = val;
    };

    /**
     * Submit function for forms - checks, collates and sends data.
     * @returns {Error}
     */
    this.submit = function () {
      var
        title = $scope.title,
        otherTitle = $scope.title,
        name = $scope.name,
        dob = $scope.dob,
        location = $scope.location,
        datetime = $scope.datetime,
        feedback = $scope.feedback;

      console.log($scope, title);
      // Only act if the form is valid
      if(
        // Check inputs have been used first.
        typeof title === 'undefined' || (title === 'Other' && typeof otherTitle === 'undefined') || typeof name === 'undefined' || typeof dob === 'undefined' || typeof location === 'undefined' || typeof datetime === 'undefined' ||

        //Check input validity using HTML5 Forms
        (title.checkValidity && !title.checkValidity()) ||
        (title == 'Other' && !otherTitle.checkValidity()) ||
        (name.checkValidity && !name.checkValidity()) ||
        (dob.checkValidity && !dob.checkValidity()) ||
        (location.checkValidity && !location.checkValidity()) ||
        (datetime.checkValidity && !datetime.checkValidity()) ||
        (typeof feedback !== 'undefined' && feedback.checkValidity && !feedback.checkValidity())
      ) {

        this.triedSet(true);
        this.stepSet(6);
        return new Error('Form not valid');

      }

      var
        ret = {
          "Title" : (title === 'Other') ? otherTitle : title,
          "Name" : name,
          "Date of Birth" : dob,
          "Location" : location,
          "Date and Time" : datetime,
          "Feedback" : feedback
        };

      // Log output to console
      console.log(ret);

      // Save output to Putsreq.com
      var putsReqURI = 'https://putsreq.com/DzyZpdaH94kFVaKBFehZ', ctrl = this;

      $http.post(putsReqURI, {'JSON' : JSON.stringify(ret)})
        .success(function () {
          // Show success message
          ctrl.stepSet(4);
        })
        .error(function () {
          // Something went wrong with request
          ctrl.stepSet(5);
        });
    }

  });
